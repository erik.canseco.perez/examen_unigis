package com.example.examenunigis.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "tipoAutomovil")
public class TipoAutomovilEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;


    @ColumnInfo(name = "tipo_automovil")
    public int tipo_automovil;
    @ColumnInfo(name = "precion_minuto")
    public float precion_minuto;

}
