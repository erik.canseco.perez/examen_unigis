package com.example.examenunigis.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.examenunigis.db.dao.VehiculosDao;
import com.example.examenunigis.db.entity.BitacoraEntity;
import com.example.examenunigis.db.entity.TipoAutomovilEntity;
import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {VehiculosEntity.class,BitacoraEntity.class,TipoAutomovilEntity.class},version = 1,exportSchema = false)
public abstract class VehiculoRoomDatabase extends RoomDatabase  {
    public abstract VehiculosDao vehiculosDao();
    private  static volatile VehiculoRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public  static VehiculoRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (VehiculoRoomDatabase.class){
                if (INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                            VehiculoRoomDatabase.class,"veiculos_database")
                            .build();
                }
            }
        }
        return INSTANCE;

    }
}
