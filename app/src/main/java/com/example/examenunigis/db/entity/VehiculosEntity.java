package com.example.examenunigis.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "vehiculos")
public class VehiculosEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "placa")
    public String placa;
    @ColumnInfo(name = "telefono")
    public String telefono;
    @ColumnInfo(name = "tipo_veiculo")
    public int tipo_veiculo ;


    public VehiculosEntity(String placa, String telefono, int tipo_veiculo) {
        this.placa = placa;
        this.telefono = telefono;
        this.tipo_veiculo = tipo_veiculo;
    }

}
