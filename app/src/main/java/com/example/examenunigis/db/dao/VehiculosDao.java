package com.example.examenunigis.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.List;

@Dao
public interface VehiculosDao {

    @Query("Select * from Vehiculos where tipo_veiculo= :tipoVeiculo")
    LiveData<List<VehiculosEntity>> getvehiculos (int tipoVeiculo);

    @Query("Select * from Vehiculos")
    LiveData<List<VehiculosEntity>> getAllvehiculos ();

    @Insert
    void insertVehiculo(VehiculosEntity vehiculo);
}
