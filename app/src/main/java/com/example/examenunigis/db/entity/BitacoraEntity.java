package com.example.examenunigis.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "Bitacora")
public class BitacoraEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "placa")
    public String placa;
    @ColumnInfo(name = "fechaInicial")
    public Long fechaInicial;
    @ColumnInfo(name = "fechaFinal")
    public Long fechaFinal ;

    public BitacoraEntity(String placa, Long fechaInicial, Long fechaFinal) {
        this.placa = placa;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
    }
}
