package com.example.examenunigis;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.examenunigis.common.Constantes;
import com.example.examenunigis.data.VehiculosViewModel;
import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.ArrayList;
import java.util.List;

public class RegistroEntrada extends AppCompatActivity {

    private RecyclerView recyclerViewVeiculo;
    private MyItemRecyclerViewAdapter adapter;
    private VehiculosViewModel vehiculoViewModel;
    private List<VehiculosEntity> listVehiculo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_entrada);
        recyclerViewVeiculo = findViewById(R.id.rv_vehiculos);

        vehiculoViewModel= ViewModelProviders.of(this).get(VehiculosViewModel.class);
        recyclerViewVeiculo.setLayoutManager(new LinearLayoutManager(this));
        listVehiculo= new ArrayList<VehiculosEntity>();
        adapter = new MyItemRecyclerViewAdapter(listVehiculo,this);
        recyclerViewVeiculo.setAdapter(adapter);
        loadVehiculoData();
    }
    private void loadVehiculoData(){
        vehiculoViewModel.getAllVehiculo().observe(this, new Observer<List<VehiculosEntity>>() {
            @Override
            public void onChanged(List<VehiculosEntity> vehiculosEntities) {
                listVehiculo=vehiculosEntities;
                adapter.setNuevosVehiculos(vehiculosEntities);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.munu_vehiculo,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId()==R.id.id_agregar){
            NuevoVehiculoDialogFragment nuevoVehiculoDialogFragment=NuevoVehiculoDialogFragment.newInstance(Constantes.TipoVehiculo.OFICIAL.ordinal());
            nuevoVehiculoDialogFragment.show(getSupportFragmentManager(),"NuevoVehiculoDialogFragment");
        }
        return true;
    }
}