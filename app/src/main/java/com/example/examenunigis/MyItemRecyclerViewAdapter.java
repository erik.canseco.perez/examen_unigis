package com.example.examenunigis;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.List;


public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private List<VehiculosEntity> mValues;
    private Context context;

    public MyItemRecyclerViewAdapter(List<VehiculosEntity> items, Context context) {
        mValues = items;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_vehiculos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mPlaca.setText(holder.mItem.placa);

    }

    public void setNuevosVehiculos(List<VehiculosEntity> vehiculos){
        this.mValues=vehiculos;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public  TextView mPlaca;
        public  VehiculosEntity mItem;

        public ViewHolder(View view) {
            super(view);
            mPlaca = view.findViewById(R.id.tv_placa);
        }
    }
}