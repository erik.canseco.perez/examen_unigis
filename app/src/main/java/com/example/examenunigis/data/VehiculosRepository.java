package com.example.examenunigis.data;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.examenunigis.db.VehiculoRoomDatabase;
import com.example.examenunigis.db.dao.VehiculosDao;
import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.List;

public class VehiculosRepository {


    private static VehiculosDao vehiculosDao;
    private static LiveData<List<VehiculosEntity>> vehiculos;



    public VehiculosRepository(Application application) {
        VehiculoRoomDatabase db= VehiculoRoomDatabase.getDatabase(application);
        vehiculosDao=db.vehiculosDao();
    }


    public LiveData<List<VehiculosEntity>> getAllVeiculos(int tipo_vehiculo){
        vehiculos = vehiculosDao.getvehiculos(tipo_vehiculo);
        return vehiculos;
    }

    public static void crearVehiculo(VehiculosEntity vehiculo) {
        VehiculoRoomDatabase.databaseWriteExecutor.execute(()->{
            vehiculosDao.insertVehiculo(vehiculo);
            vehiculos=vehiculosDao.getvehiculos(vehiculo.tipo_veiculo);
        });

    }

    public LiveData<List<VehiculosEntity>> getAllVeiculos(){
        vehiculos = vehiculosDao.getAllvehiculos();
        return vehiculos;
    }





}
