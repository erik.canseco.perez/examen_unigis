package com.example.examenunigis.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.List;

public class VehiculosViewModel extends AndroidViewModel {
    private VehiculosRepository vehiculosRepository;

    public VehiculosViewModel(@NonNull Application application) {
        super(application);
        vehiculosRepository = new VehiculosRepository(application);
    }

    public LiveData<List<VehiculosEntity>> getVehiculo(int tipo_vehiculo){
        return vehiculosRepository.getAllVeiculos(tipo_vehiculo);
    }

    public void crearVehiculo(VehiculosEntity vehiculo){

        VehiculosRepository.crearVehiculo(vehiculo);
    }

    public LiveData<List<VehiculosEntity>> getAllVehiculo(){
        return vehiculosRepository.getAllVeiculos();
    }

}
