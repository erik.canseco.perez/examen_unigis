package com.example.examenunigis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_registro_entrada,bt_registro_salida,bt_veiculos_oficiales,bt_veiculos_recidente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_veiculos_recidente = findViewById(R.id.bt_vehiculo_recidencial);
        bt_veiculos_oficiales = findViewById(R.id.bt_vehiculo_oficial);
        bt_registro_entrada = findViewById(R.id.bt_registro_entrada);
        bt_registro_salida = findViewById(R.id.bt_registro_salida);

        bt_veiculos_oficiales.setOnClickListener(this);
        bt_veiculos_recidente.setOnClickListener(this);
        bt_registro_entrada.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_vehiculo_recidencial:
                getVeiculoRecidencial();
                break;
            case R.id.bt_vehiculo_oficial:
                getVeiculoOficial();
                break;
            case R.id.bt_registro_entrada:
                getRegistro();
                break;
            default:

                break;
        }
    }

    private void getVeiculoRecidencial() {
        Intent intent = new Intent(this, RegistroVehiculosRecidente.class);
        startActivity(intent);
    }

    private void getVeiculoOficial() {
        Intent intent = new Intent(this, RegistroVehiculosOficiales.class);
        startActivity(intent);
    }
    private void getRegistro(){
        Intent intent = new Intent(this, RegistroEntrada.class);
        startActivity(intent);
    }



}