package com.example.examenunigis;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.examenunigis.common.Constantes;
import com.example.examenunigis.data.VehiculosViewModel;
import com.example.examenunigis.db.entity.VehiculosEntity;

import java.util.Date;

public class NuevoVehiculoDialogFragment extends DialogFragment {

    EditText et_placa,et_telefono;
    Button bt_guardar;
    ImageView iv_close;
    int tipo_vehiculo;

    static NuevoVehiculoDialogFragment newInstance(int tipo_vehiculo) {
        NuevoVehiculoDialogFragment f = new NuevoVehiculoDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt(Constantes.TIPO_VEHICULO, tipo_vehiculo);
        f.setArguments(args);

        return f;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        tipo_vehiculo = getArguments().getInt(Constantes.TIPO_VEHICULO);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_dialog_nuevo_vehiculo, container, false);
         et_placa = v.findViewById(R.id.et_placa);
         et_telefono = v.findViewById(R.id.et_telefono);

         if(tipo_vehiculo == Constantes.TipoVehiculo.RECIDENCIAL.ordinal()){
             et_telefono.setVisibility(View.VISIBLE);
         }

         bt_guardar = v.findViewById(R.id.bt_guardar);
         iv_close = v.findViewById(R.id.iv_close);

         iv_close.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(!et_placa.getText().toString().isEmpty()) {
                     showDialogConfirm().show();
                 }else{
                     getDialog().dismiss();
                 }
             }
         });

         bt_guardar.setOnClickListener(view -> {
             Guardar();
         });

        return v;
    }


    private void Guardar() {
        String placa = et_placa.getText().toString();
        String telefono =et_telefono.getText().toString();


        if(placa.isEmpty()){
            et_placa.setError("El campo de placa es obligatorio.");
        }else if(telefono.isEmpty() & tipo_vehiculo == Constantes.TipoVehiculo.RECIDENCIAL.ordinal()) {
            et_telefono.setError("El campo de placa es obligatorio.");
        }else{
            VehiculosViewModel vehiculosViewModel = new ViewModelProvider(getActivity()).get(VehiculosViewModel.class);
            telefono= Constantes.TipoVehiculo.RECIDENCIAL.ordinal() == tipo_vehiculo ? et_telefono.getText().toString():"";
            VehiculosEntity vehiculosEntity =new VehiculosEntity(placa,telefono,tipo_vehiculo);
            vehiculosViewModel.crearVehiculo(vehiculosEntity);
            getDialog().dismiss();
        }
    }

    private AlertDialog showDialogConfirm(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getDialog().dismiss();

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        return dialog;

    }

}
