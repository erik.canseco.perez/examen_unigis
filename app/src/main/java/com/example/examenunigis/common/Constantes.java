package com.example.examenunigis.common;

public class Constantes {

        public static final String TIPO_VEHICULO = "TIPO_VEHICULO";
        public static final String PREF_USER = "PREF_USER";
        public static final String ARG_TAREA_ID = "TAREA_ID";

    public static enum TipoVehiculo{
        OFICIAL(0.0f),
        RECIDENCIAL(0.05f),
        NORECIDENCIAL(0.5f);

        private float precioxminuto;


        TipoVehiculo(float s) {
            precioxminuto=s;
        }

        float getprecioxminuto(){return precioxminuto;}
    }

}
